![Did you forget something?](kym-cdn.com/photos/images/original/001/515/252/89e.jpeg)
# It is always those tiny little things that make a huge difference
Making working with Git in Python projects more easy. 
Instead of having the whole usual Git command hustle, how about using something like *black-push*?
## Why?
Format, add, commit, and push your files with one single command. 

## How?
Install necessary libraries in your environment
```
pip install black pycln flake8
```

Download the file *blkp*
* First, run

    ```chmod u+x blkp ```

* Then, place black_push, e.g in */usr/bin/*. In the directory where *black_push* lies run 
  ```sudo mv blkp /usr/bin/```
* Now, you can use 

    ```blkp```

 * Enjoy your countless hours of free tieme
